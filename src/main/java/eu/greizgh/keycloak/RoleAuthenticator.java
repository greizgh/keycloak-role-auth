package eu.greizgh.keycloak;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.models.AuthenticatorConfigModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.RoleModel;

public class RoleAuthenticator implements Authenticator {
    public static final String ROLE = "role";

    private static final Logger logger = Logger.getLogger(RoleAuthenticator.class);

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        AuthenticatorConfigModel configModel = context.getAuthenticatorConfig();
        String requiredRole = configModel.getConfig().get(ROLE);
        RealmModel realm = context.getRealm();
        UserModel user = context.getUser();
        RoleModel role = realm.getRole(requiredRole);

        if (role == null) {
            logger.errorv("Invalid role name submitted: {0}", requiredRole);
            context.failure(AuthenticationFlowError.INTERNAL_ERROR);
            return;
        }

        if (!user.hasRole(role)) {
            logger.infof(
                    "Access denied because of missing role. realm=%s username=%s role=%s",
                    realm.getName(),
                    user.getUsername(),
                    requiredRole
            );
            context.failure(AuthenticationFlowError.INVALID_USER);
            return;
        }

        context.success();
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public boolean requiresUser() {
        return true;
    }

    @Override
    public void action(AuthenticationFlowContext context) {
    }

    @Override
    public void close() {
    }
}
