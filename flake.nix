{
  description = "A keycloak authenticator enforcing role";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";
    flake-utils.url = "github:numtide/flake-utils";
    mvn2nix = {
      url = "github:fzakaria/mvn2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, mvn2nix }:

    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ mvn2nix.overlay ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        mavenRepository = pkgs.buildMavenRepositoryFromLockFile {
          file = ./mvn2nix-lock.json;
        };
      in
      rec {
        packages =
          {
            keycloak-role-auth = pkgs.stdenv.mkDerivation

              rec {
                pname = "keycloak-role-auth";
                version = "1.0";
                name = "${pname}-${version}";
                src = pkgs.lib.cleanSource ./.;

                buildInputs = with pkgs; [ maven ];
                buildPhase = ''
                  echo "Building with maven repository ${mavenRepository}"
                  mvn package --offline -Dmaven.repo.local=${mavenRepository}
                '';

                installPhase = ''
                  # create the out directory
                  mkdir -p $out

                  # copy out the JAR
                  # Maven already setup the classpath to use m2 repository layout
                  # with the prefix of lib/
                  cp target/${name}.jar $out/
                '';
              };
          };

        defaultPackage = packages.keycloak-role-auth;
      }
    );
}
